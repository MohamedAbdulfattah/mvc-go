package main

import (
	"fmt"
	"net/http"
	"os"
	"todo/controller"
	"todo/models"

	_ "github.com/go-sql-driver/mysql" // mysql driver
)

func main() {
	mux := controller.Register()
	db := models.Connect()
	defer db.Close()
	fmt.Println("Serving ...")
	port := os.Getenv("PORT")
	http.ListenAndServe(":"+port, mux)
}
