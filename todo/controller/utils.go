package controller

import (
	"encoding/json"
	"net/http"
)

func RespWithMsg(rw http.ResponseWriter, body interface{}, statusCode int) {
	rw.Header().Add("Content-Type", "application/json")
	rw.WriteHeader(statusCode)
	json.NewEncoder(rw).Encode(body)
}
