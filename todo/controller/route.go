package controller

import (
	"github.com/gorilla/mux"
)

func Register() *mux.Router {
	// mux := http.NewServeMux()
	// mux.HandleFunc("/ping", ping())
	// mux.HandleFunc("/todo/create", create())
	// mux.HandleFunc("/todo/get-all", reads())
	// mux.HandleFunc("/todo/get-by-name", reads())

	mux := mux.NewRouter()
	mux.HandleFunc("/ping", ping()).Methods("GET")
	mux.HandleFunc("/todo/create", create()).Methods("POST")
	mux.HandleFunc("/todo/get-all", reads()).Methods("GET")
	mux.HandleFunc("/todo/get-by-name", readByName()).Methods("GET").Queries("name", "{name}")
	mux.HandleFunc("/todo/delete", readByName()).Methods("DELETE").Path("/{name}")
	return mux
}
