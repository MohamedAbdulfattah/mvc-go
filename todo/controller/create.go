package controller

import (
	"encoding/json"
	"fmt"
	"net/http"
	"todo/models"
	"todo/views"
)

func create() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodPost {
			// take some data
			var body views.PostRequest
			err := json.NewDecoder(r.Body).Decode(&body)
			if err != nil {
				rw.WriteHeader(http.StatusBadRequest)
				// rw.Header().Add("Content-Type", "application/json")
				// json.NewEncoder(rw).Encode("Invalid request")

				RespWithMsg(rw, "Invalid request", http.StatusBadRequest)
				return
			}
			// save it!
			if err := models.CreateTodo(body.Name, body.Todo); err != nil {
				// rw.Write([]byte("Some error"))
				RespWithMsg(rw, err.Error(), http.StatusInternalServerError)
				return
			}

			RespWithMsg(rw, "todo created successfully!", http.StatusCreated)
			return
		}
	}
}

func delete() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodDelete {
			name := r.URL.Query().Get("name")
			fmt.Println(name)
			err := models.DeleteTodo(name)
			if err != nil {
				RespWithMsg(rw, err.Error(), http.StatusInternalServerError)
			}
			RespWithMsg(rw, "deleted succesfully", http.StatusOK)
			return
		}
	}
}
