package controller

import (
	"encoding/json"
	"net/http"
	"todo/views"
)

func ping() http.HandlerFunc {
	handler := func(w http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodGet {
			data := views.Response{
				Code: http.StatusOK,
				Body: "pong",
			}
			w.WriteHeader(http.StatusOK)
			w.Header().Add("Content-Type", "application/json")
			json.NewEncoder(w).Encode(data)
		}
	}
	return handler
}
