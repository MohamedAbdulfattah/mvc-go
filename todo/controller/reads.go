package controller

import (
	"fmt"
	"net/http"
	"todo/models"
)

func reads() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodGet {
			todos, err := models.ReadAll()
			if err != nil {
				RespWithMsg(rw, err.Error(), http.StatusInternalServerError)
			}
			RespWithMsg(rw, todos, http.StatusOK)
			return
		}
	}
}

func readByName() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodGet {
			name := r.URL.Query().Get("name")
			fmt.Println(name)
			todos, err := models.ReadByName(name)
			if err != nil {
				RespWithMsg(rw, err.Error(), http.StatusInternalServerError)
			}
			RespWithMsg(rw, todos, http.StatusOK)
			return
		}
	}
}
