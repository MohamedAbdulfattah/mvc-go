package models

func CreateTodo(name, todo string) error {
	insertQ, err := con.Query("INSERT INTO Todo VALUES(?,?)", name, todo)
	defer insertQ.Close()
	if err != nil {
		return err
	}
	return nil
}
func DeleteTodo(name string) error {
	deleteQ, err := con.Query("DELETE FROM Todo WHERE name=?", name)
	defer deleteQ.Close()
	if err != nil {
		return err
	}
	return nil
}
