package models

import (
	"database/sql"
	"fmt"
	"log"
)

var con *sql.DB

func Connect() *sql.DB {
	dataSourceName := "admin:Moha#1996@(127.0.0.1:3306)/todo_list?parseTime=true"
	db, err := sql.Open("mysql", dataSourceName)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connected to Db...")
	con = db
	return db
}
